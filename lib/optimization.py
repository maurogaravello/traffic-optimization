#!/usr/bin/env python

#######################################
# optimizazion.py
#
# Function for finding an optimal control
# 
# 
#######################################

import numpy
import save
import os
import cost
import ibvp
import logging
from datetime import datetime

def initialize_control(DirName, m, tt, theta):
    """This function initialize the controls

    :param DirName: string contain the path of the simulation directory

    :param m: number of incoming roads

    :param tt: vector of times for the simulation

    :param theta: point of maximum for the flux. The controls are
                  constantly equal to theta
    """
    
    ControlName = 'controls_000'

    control = numpy.array([theta * numpy.ones_like(tt) for i in xrange(m)])

    
    save.Control_Save(DirName, ControlName, control)
    return control



def partial_optimization(index, control, f, N, M, tt, cost_J, junction, c_tv):
    """
    Optimization procedure for one control.

    :param index: index for the road where optimization occurs
    :param control: dictionary containig the initial controls
    :param f: flux class
    :param N: number of points for the control range
    :param M: number of intervals where the control is constant.
              M should be less than tt.size
    :param tt: times of simulation
    :param cost_J: function. Integrand of the cost
    :param junction: junction class
    :param c_tv: float. Coefficient of TV
    """

    assert(index == 0 or index == 1)
    assert(M < tt.size)

    # print N
    # control_range = numpy.union1d(numpy.linspace(f.theta, f.rho_max, N),
    #                               control[0])
    # control_range = numpy.union1d(control_range, control[1])

    control_range = numpy.linspace(f.theta, f.rho_max, N)
    # print numpy.size(control_range)

    # control[0][0] = 1.
    # control[1][0] = 1.
    if index == 0:
        ctrl = [numpy.copy(control[0]), control[1]]
    else:
        ctrl = [control[0], numpy.copy(control[1])]

    delta = tt[-1] / M

    for i in xrange(M - 1, -1, -1): # tolto per patch
    # for i in xrange(M - 1, 0, -1):
        # print i
        mask = (tt >= i*delta) & (tt < (i+1)*delta)
        
        tmp = numpy.array([])
        for j in control_range:
            ctrl[index][mask] = j
            tmp = numpy.append(tmp, cost.cost(ibvp.find_outflow(
                junction.incoming, junction.outgoing, junction.matrix, tt, ctrl, f),
                                              tt, cost_J, c_tv))

        ctrl[index][mask] = control_range[numpy.argmax(tmp)]
        #print control_range[numpy.argmax(tmp)]
        # print tmp
        #exit(1)
        
    return ctrl



def optimization(DirName, tt, f, cycles, N, M, cost_J, junction, c_tv, number_file = 0):
    """Optimization procedure.

    :param DirName: string contain the path of the simulation directory
    :param m: number of incoming roads
    :param tt: numpy vector of times for the simulation
    :param f: flux class
    :param cycles: number of cycles of optimizations
    :param number_file: the number of file where the control
                        is saved. The default is 0. In this case
                        the initialization procedure is applied.

    :param N: number of points for the control range
    :param M: number of intervals where the control is constant.
              M should be less than tt.size

    :param cost_J: function. Integrand of the cost
    :param junction: junction class
    :param c_tv: float. Coefficient in front of the total variation.
    """


    if number_file == 0:
        control = initialize_control(DirName, junction.m, tt, f.theta)
    else:
        ControlName = 'controls_' + str('%03d'%(number_file)) + '.npz'
        npzf = numpy.load(os.path.join(DirName, ControlName))
        # control = numpy.array([theta * numpy.ones_like(tt) for i in xrange(m)])
        # control = numpy.array([npzf['of1'], npzf['of2']])
        control = npzf['c']
        npzf.close()


    for cyc in xrange(number_file, number_file + cycles):
        
        logging.info('optimize.py: ' + str(cyc + 1) + ' cicle started at ' + str(datetime.now()))

        
        control = partial_optimization(0, control, f, N, M, tt, cost_J, junction, c_tv)
    
        control = partial_optimization(1, control, f, N, M, tt, cost_J, junction, c_tv)


        # [b1, b2] = ibvp.find_outflow(junction.incoming, junction.outgoing, junction.matrix, tt, control, f)
         
        # control = [f.right_inverse(b1), f.right_inverse(b2)] # density
        ControlName = 'controls_' + str('%03d'%(cyc + 1))

        save.Control_Save(DirName, ControlName, control)
        
        logging.info('optimize.py: ' + str(cyc + 1) + ' cicle finished at ' + str(datetime.now()))

#
# classical_to_control function
#
def classical_to_control(DirName, f):
    """ This function creates the file
    'controls_001.npz' as a base for the optimization
    procedure from the outflow of the classical solution.

    :param DirName: string. Name of the simulation directory.
    :param f: flux class
    """

    ControlName = 'classical-outflow.npz'
    OutputName = 'controls_001.npz'
    npzf = numpy.load(os.path.join(DirName, ControlName))
    control = numpy.array([f.right_inverse(npzf['of1']), f.right_inverse(npzf['of2'])])
    npzf.close()
    

    save.Control_Save(DirName, OutputName, control)
