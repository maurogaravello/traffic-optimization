#!/usr/bin/env python

#######################################
# cost.py
#
# Function for the evaluation of the cost
# of a solution
# 
#######################################

import numpy
import ibvp
import os

def cost(outflow, tt, cost_J, c = 0):
    """ This function computes the cost of the solution.
    :param outflow: tuple containing numpy array.
                    The size of the tuple is the number of incoming roads.
                    Each element is the outflow (flux) of the solution in the
                    corresponding incoming road
    :param tt: numpy array of the times. Same size of each element of outflow
    :param cost_J: function given the integrand for the cost
    :para c: coefficient in front of the TV. Default is 0.

    """
    assert(outflow[0].size == tt.size)
    
    f = cost_J(outflow)

    if c > 0:
        f2 = [tv(outflow[i]) for i in xrange(len(outflow))]
        c = c * sum(f2)

    return numpy.trapz(f, tt) - c



def find_cost(junction, control, A, tt, flux, cost_J):
    """ This function returns the cost for a given control

    :param junction: junction class
    :param control: list of boundary data
    :param A: class containing the distribution coefficients a31, a32, a41, a42.
    :param tt: numpy array containing the time of simulations.
    :param flux: flux class
    :param cost_J: function given the integrand for the cost
    
    """

    return cost(ibvp.find_outflow(junction.incoming,
                                  junction.outgoing, A, tt, control,
                                  flux), tt, cost_J)


def tv(u):
    """ This function calculates the total variation of an array

    :param u: numpy array

    :out: a real positive number
    """
    u = u[1:]
    return numpy.sum(numpy.fabs(u[:-1] - u[1:]))


def find_tv(junction, control, A, tt, flux):
    """ This function returns the cost for a given control

    :param junction: junction class
    :param control: list of boundary data
    :param A: class containing the distribution coefficients a31, a32, a41, a42.
    :param tt: numpy array containing the time of simulations.
    :param flux: flux class
    
    """
    out = ibvp.find_outflow(junction.incoming,
                            junction.outgoing, A, tt, control,
                            flux)

    return [tv(out[i]) for i in xrange(len(out))]



def find_classical_cost(junction, A, flux, DirName, tt, cost_J):
    """ This function returns the cost for the classical solution

    :param junction: junction class
    :param A: class containing the distribution coefficients a31, a32, a41, a42.
    :param DirName: string. Name of the simulation directory
    :param tt: numpy array containing the time of simulations.
    :param cost_J: function given the integrand for the cost
    :param flux: flux class

    It returns a triple (c1, c2, c3) where:
                                     c1 -> cost without total variation
                                     c2 -> total variation of the outflux 1
                                     c3 -> total variation of the outflux 2
    
    
    """
    FileName = os.path.join(DirName, 'classical-outflow.npz')

    ff = numpy.load(FileName)
    outflow1 = ff['of1']
    outflow2 = ff['of2']
    ff.close()

    # control = [flux.right_inverse(outflow1), flux.right_inverse(outflow2)]

    # (outflow1, outflow2) = ibvp.find_outflow(junction.incoming,
    #                         junction.outgoing, A, tt, control,
    #                         flux)

    return (cost((outflow1, outflow2), tt, cost_J), tv(outflow1), tv(outflow2))
