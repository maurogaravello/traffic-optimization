#!/usr/bin/env python

#######################################
# junction.py
#
# Functions for creating a junction of roads
# the numerical simulation
#
#
#######################################


import numpy
import road



class Junction(object):

    def __init__(self, m, n, roads, A, dx, Initial_Datum):
        """
        :param m: int given the number of incoming roads
        :param n: int given the number of outgoing roads
        :param roads: list or tuples. Each element is a tuple of
                      two floats giving the boundary of a road.
        :param A: class for the distribution matrix.
        :param dx: scalar for spatial mesh
        :param Initial_Datum: list of function giving the initial
                              datum
        """
        assert (len(roads) == n+m and dx >0 and n >= 1 and m>=1)

        self.incoming = [road.road(roads[i][0],roads[i][1], dx, Initial_Datum[i])
                         for i in xrange(m)]
        self.outgoing = [road.road(roads[i][0],roads[i][1], dx,
                                   Initial_Datum[i]) for i in xrange(m,n+m)]
        self.m = m
        self.n = n
        self.dx = dx
        self.matrix = A


class DistributionMatrix(object):

    def __init__(self, A):
        
        self.a31 = A[0][0]
        self.a32 = A[0][1]
        self.a41 = A[1][0]
        self.a42 = A[1][1]
        self.matrix = numpy.array(A)
        self.inverse = numpy.linalg.inv(self.matrix)
        
