#!/usr/bin/env python

#######################################
# ibvp.py
#
# Function for the evaluation of a solution
# in a road of a junction
# 
#######################################


import numpy
import LaxFr
import Godunov
import save

def trace(a,b,flux):
    if a<b and flux.flux(a) < flux.flux(b):
        return (a,a)
    if a<b and flux.flux(a) > flux.flux(b):
        return (b,b)
    if a<b and flux.flux(a) == flux.flux(b):
        return (a,b)
    if a == b:
        return (a,b)
    if a < flux.theta:
        return (a,a)
    if b > flux.theta:
        return (b,b)
    return (flux.theta, flux.theta)



def Solution_Incoming(InitialDatum, domain, flux, tt, boundary_datum, DirName, saving=True, number_saving = 80):
    """
    This function solves a initial-boundary value problem for the
    conservation law
    \pt u + \px f(u) = 0
    in an incoming road. The left boundary is free flow.
    The output is the outdensity

    
    :param InitialDatum: numpy array giving the initial condition
    :param domain: numpy array giving the space domain.
                   It has the same shape of InitialDatum
                   One ghost cell at the left and one at the right
    :param flux: flux class
    :param tt: numpy array containing all the simulation times
    :param boundary_datum: numpy array containing the boudary data
                           boundary as a density
                           It has the same shape as tt
    :param DirName: string giving the directory of the simulation
    :param saving: optional boolean. Default = True (save the simulation)
    :param number_saving: optional int. Default = 80. Gives the number
                          (possibly an upper bound) for
                          the number of savings
    """

    assert(boundary_datum.size == tt.size and InitialDatum.size == domain.size)

    outdensity = numpy.array([trace(InitialDatum[-1], boundary_datum[0], flux)[0]])
    
    dx = domain[1] - domain[0]
    dt = tt[1] - tt[0]

    u = InitialDatum
    j = 0 # counter for saving
    ttp = numpy.linspace(0, tt[-1], number_saving)
    
    for i in xrange(1,tt.size):
        t = tt[i]
        bb = boundary_datum[i]
        #u[-1] = bb

        if saving and t >= ttp[j]:
            save.Sol_Save(DirName, str("%05d" %j), t, u)
            j += 1

  
        # u = LaxFr.C_step(u, ('f', bb), flux, dt/dx)
        u = Godunov.C_step(u, ('f', bb), flux, dt/dx)
        outdensity = numpy.append(outdensity, trace(u[-1], bb, flux)[0])

    assert(outdensity.size == tt.size)


    return outdensity
# end of Solution_Incoming



def Solution_Outgoing(InitialDatum, domain, flux, tt, boundary_datum, DirName, saving=True, number_saving = 80):
    """
    This function solves a initial-boundary value problem for the
    conservation law
    \pt u + \px f(u) = 0
    in an outgoing road. The right boundary is free flow.
    The output is the outdensity

    
    :param InitialDatum: numpy array giving the initial condition
    :param domain: numpy array giving the space domain.
                   It has the same shape of InitialDatum
                   One ghost cell at the left and one at the right
    :param flux: flux class
    :param tt: numpy array containing all the simulation times
    :param boundary_datum: numpy array containing the boundary data
                           boundary as a density
                           It has the same shape as tt
    :param DirName: string giving the directory of the simulation
    :param saving: optional boolean. Default = True (save the simulation)
    :param number_saving: optional int. Default = 80. Gives the number
                          (possibly an upper bound) for
                          the number of savings
    """

    assert(boundary_datum.size == tt.size and InitialDatum.size == domain.size)

    outdensity = numpy.array([trace(boundary_datum[0], InitialDatum[0], flux)[1]])
    
    dx = domain[1] - domain[0]
    dt = tt[1] - tt[0]

    u = InitialDatum
    j = 0 # counter for saving
    ttp = numpy.linspace(0, tt[-1], number_saving)
    
    for i in xrange(1,tt.size):
        t = tt[i]
        bb = boundary_datum[i]
        # u[0] = bb

        if saving and t >= ttp[j]:
            save.Sol_Save(DirName, str("%05d" %j), t, u)
            j += 1

  
        # u = LaxFr.C_step(u, (bb, 'f'), flux, dt/dx)
        u = Godunov.C_step(u, (bb, 'f'), flux, dt/dx)
        outdensity = numpy.append(outdensity, trace(bb, u[0], flux)[1])

    assert(outdensity.size == tt.size)


    return outdensity
# end of Solution_Outgoing


def find_outflow(incoming_roads, outgoing_roads, A, tt, boundary_data, flux):
    """
    This function gives the outflow for incoming roads,
    for a junction with *two* incoming
    and *two* outgoing roads.
    More precisely it returns a tuple containing
    the boundary flux for the incoming roads.

    :param incoming_roads: list of incoming roads. Each element
                           of the list is an object of type road.

    :param outgoing_roads: list of outgoing roads. Each element
                           of the list is an object of type road.

    :param A: class containing the distribution coefficients a31, a32, a41, a42.

    :param tt: numpy array containing the time of simulations.

    :param boundary_data: list of boundary data. It has the same
                          size as the number of incoming roads.
                          Each element is a numpy array of the
                          same size of tt
                           
    :param flux: flux class

    
    """

    m = len(incoming_roads)
    n = len(outgoing_roads)

    assert(m == len(boundary_data) and m == 2 and n == 2)

    assert(tt.size == boundary_data[i].size for i in xrange(m))
    

    b1 = [flux.flux(Solution_Incoming(incoming_roads[i].initial_datum,
                                      incoming_roads[i].domain, flux,
                                      tt, boundary_data[i], 'not',
                                      False)) for i in xrange(m)]


    #    flux_out = numpy.minimum(reduce(numpy.add, b1), flux.maximum)
    flux_out = [numpy.minimum(A.a31 * b1[0] + A.a32 * b1[1], flux.maximum), 
                numpy.minimum(A.a41 * b1[0] + A.a42 * b1[1], flux.maximum)]

    #    print flux_out


    b2 = [flux.flux(Solution_Outgoing(outgoing_roads[i].initial_datum,
                                      outgoing_roads[i].domain, flux,
                                      tt, flux.left_inverse(flux_out[i]),
                                      'not', False)) for i in xrange(n)]

    cc = [A.inverse[0,0] * b2[0] + A.inverse[0,1] * b2[1],
          A.inverse[1,0] * b2[0] + A.inverse[1,1] * b2[1]]

    c1 = numpy.minimum(b1[0], cc[0]) 
    c2 = numpy.minimum(b1[1], cc[1])

    return ((c1, c2))






def Solution_Junction(InitialDatum, junc, domain, A, flux, tt, control, DirName, number_saving):
    """
    This function calculates a initial-boundary value problem for the
    conservation law
    \pt u + \px f(u) = 0
    in all the junction and save the profiles.
    
    :param InitialDatum: list of numpy arrays giving the initial condition
    :param junc: junction class
    :param domain: list of numpy arrays giving the space domain.
                   It has the same shape of InitialDatum
                   Each element has one ghost cell at the left and one at the right
    :param A: matrix distribution class
    :param flux: flux class
    :param tt: numpy array containing all the simulation times
    :param control: numpy array containing the boundary data
                           boundary as a density
                           It has the same shape as tt
    :param DirName: string giving the directory of the simulation
    :param number_saving: int. Gives the number
                          (possibly an upper bound) for
                          the number of savings
    """

    assert(control[0].size == tt.size and InitialDatum[0].size == domain[0].size)

    number_saving = tt.size / number_saving 
    dx = domain[0][1] - domain[0][0]
    dt = tt[1] - tt[0]
    dtdx = dt/dx

    u1 = InitialDatum[0]
    u2 = InitialDatum[1]
    u3 = InitialDatum[2]
    u4 = InitialDatum[3]


    [b1, b2] = find_outflow(junc.incoming, junc.outgoing, A, tt, control, flux)
    control = [flux.right_inverse(b1), flux.right_inverse(b2)] # density


    u1[-1] = control[0][0]
    u2[-1] = control[1][0]

    outflux = [A.a31 * flux.flux(u1[-1]) + A.a32 * flux.flux(u2[-1]),
               A.a41 * flux.flux(u1[-1]) + A.a42 * flux.flux(u2[-1])]
    u3[0] = flux.left_inverse(outflux[0])
    u4[0] = flux.left_inverse(outflux[1])

    j = 0
    save.Sol_Save((u1, u2, u3, u4), tt[0], DirName, j) # saving the initial datum with ghost cells


    ttp = numpy.array([(i%number_saving) == 0 for i in xrange(tt.size)])
    ttp[-1] = True

    
    
    for i in xrange(1,tt.size):
        t = tt[i]
        bb1 = control[0][i] # boundary first road
        bb2 = control[1][i] # boundary second road

        u1 = Godunov.C_step(u1, ('f', bb1), flux, dtdx)
        u2 = Godunov.C_step(u2, ('f', bb2), flux, dtdx)

        outflux = [A.a31 * flux.flux(u1[-1]) + A.a32 * flux.flux(u2[-1]),
                   A.a41 * flux.flux(u1[-1]) + A.a42 * flux.flux(u2[-1])]

        u3 = Godunov.C_step(u3, (flux.left_inverse(outflux[0]), 'f'), flux, dtdx)
        u4 = Godunov.C_step(u4, (flux.left_inverse(outflux[1]), 'f'), flux, dtdx)

        
        if ttp[i]:
            j += 1
            save.Sol_Save((u1, u2, u3, u4), tt[i], DirName, j)
  
    return 1
# end of Solution_Junction
