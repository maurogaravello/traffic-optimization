#!/usr/bin/env python

#######################################
# road.py
#
# Functions for creating road domains for
# the numerical simulation 
#######################################


import numpy



def domain(a, b, dx):
    """This function constructs a numpy vector for the domain
    which contains one ghost cell at the left and one on the right
    """
    return numpy.arange(a - dx, b + 2.*dx, dx)


class road(object):

    def __init__(self, a, b, dx, Initial_Datum):
        """
        :param a: scalar giving the left bound of the domain
        :param b: scalar giving the right bound of the domain
        :param dx: scalar for spatial mesh
        :param Initial_Datum: function giving the initial datum
        """
        assert (b > a and dx >0)
        
        self.a = a
        self.b = b
        self.dx = dx
        self.domain = domain(a, b, dx)
        self.initial_datum = Initial_Datum(self.domain)
