#!/usr/bin/env python

# saving procedures

import numpy
import os

# Saving to disk the profile of the solution
def Sol_Save(u, t, dirName, number_file):
    numpy.savez_compressed(dirName + str("%03d" %number_file) , t=t, u=u)
    # t -> time, scalar
    # u -> list of densities for roads, numpy array
    return 1


# Saving the control to disk
def Control_Save(DirName, ControlName, control):

    numpy.savez_compressed(os.path.join(DirName, ControlName), c = control)
    return 1

# Saving the outflux to the disk
def Outflux_Save(DirName, outflux1, outflux2):
    # of1 -> numpy array, outflux in road 1
    # of2 -> numpy array, outflux in road 2    
    numpy.savez_compressed(DirName, of1 = outflux1, 
                           of2 = outflux2)

    return 1
