#!/usr/bin/env python
###
### plot.py
### library for plots of the solutions computed


import matplotlib
import matplotlib.pyplot
import glob
import os
import numpy
import logging
import ibvp
from datetime import datetime
from multiprocessing import Pool


def draw(u, t, a, u_min, u_max, FName, flux = False):
    """
    This function draws two pictures. 

    :param u: list of three numpy array containing the values
              of the density at time t or the values of the
              boundary flux. Ghost cells are eliminated!!!
    :param t: float. time
    :param a: list of four numpy array containing the domains
              for the plot.
    :param u_min: list for the minimum of the plot
    :param u_max: list for the maximum of the plot
    :param FName: string containing the filename
    :param flux: boolean. If True plots the flux,
                          if False plots the densities

    """

    if flux:
        xlab = '$t$'
        ylab = '$f$'
    else:
        xlab = '$x$'
        ylab = '$u$'
        

    for i in range(2):
        matplotlib.pyplot.close(1)
        matplotlib.pyplot.figure(1)

        if flux:
            matplotlib.pyplot.plot(a[i][1:-1], u[i][1:-1], linewidth=3)
        else:
            matplotlib.pyplot.plot(a[i][1:-1], u[i][1:-1], linewidth=1)
            
        matplotlib.pyplot.xlabel(xlab).set_size('xx-large')
        matplotlib.pyplot.ylabel(ylab, rotation='horizontal').set_size('xx-large')
        if flux:
            matplotlib.pyplot.title('Outflow $I_' + str(i + 1) + '$').set_size('xx-large')
        else:
            matplotlib.pyplot.title('t = %4.2f' % t).set_size('xx-large')
        matplotlib.pyplot.axis([a[i][1], a[i][-2], u_min[i], u_max[i]])

        matplotlib.pyplot.savefig(FName + '-' + str(i + 1))



def draw_contour(t1, t2, matrix, n_color, DirName, is_classic = False):
    """ Function drawing the contour of the solution

    :param t1: numpy array describing the horizontal domain
    :param t2: numpy array describing the vertical domain
    :param matrix: tuple containing 4 numpy arrays describing the solution
    :param n_color: int. Number of colors of the plot
    :param DirName: str. Name of the directory

    This function write the file Contour.png

    """

    v = numpy.linspace(0., .9, n_color, endpoint=True)

    for i in range(4):
        matplotlib.pyplot.close(1)
        fig = matplotlib.pyplot.figure(1)

        xx, yy = numpy.meshgrid(t1[i],t2)
    
        matplotlib.pyplot.contourf(xx, yy, matrix[i], extend='max')
        matplotlib.pyplot.colorbar(format = '%.2f')
        matplotlib.pyplot.ylabel('$t$', rotation='horizontal').set_size('xx-large')
        matplotlib.pyplot.xlabel('$x$').set_size('xx-large')

        title = '$I_' + str(i+1) + '$'
        matplotlib.pyplot.title(title).set_size('xx-large')

        if is_classic:
            FileName = 'Contour-classic-I' + str(i+1) + '.png'
            matplotlib.pyplot.savefig(os.path.join(DirName, FileName))
        else:
            FileName = 'Contour-I' + str(i+1) + '.png'
            matplotlib.pyplot.savefig(os.path.join(DirName, FileName))



def plot_controls(dirName, junc, f, A, tt):
    """ plot all the controls in the directory

    :param dirName: Name of the simulation directory
    :param junc: junction class
    :param f: flux class
    :param A: class for the distribution matrix
    :param tt: numpy array with the times of the simulation
    """

    listaFiles = glob.glob(dirName + '/controls*.npz')

    for fileName in listaFiles:
        
        outName = os.path.splitext(fileName)[0]

        npzf = numpy.load(fileName)
        control = npzf['c']
        npzf.close()

        [fb1, fb2] = ibvp.find_outflow(junc.incoming, junc.outgoing, A, tt, control, f)
        
        logging.info('Plotting control ' + outName + '.png: ' + str(datetime.now()))

        draw((fb1, fb2), 0.4, (tt, tt), (-0.1, -0.1), (1.1, 1.1), outName, True)
        logging.info('Control ' + outName + '.png: plotted at ' + str(datetime.now()))



def plot_control(fileName, junc, f, A, tt):

    """ plot only one controls 

    :param FileName: Name of the control file with the path
    :param junc: junction class
    :param f: flux class
    :param A: class for the distribution matrix
    :param tt: numpy array with the times of the simulation
    """

    outName = os.path.splitext(fileName)[0]


    npzf = numpy.load(fileName)
    control = npzf['c']
    npzf.close()

    [fb1, fb2] = ibvp.find_outflow(junc.incoming, junc.outgoing, A, tt, control, f)
    # fb1 = f.flux(control[0])
    # fb2 = f.flux(control[1])

    fb3 = A.a31 * fb1 + A.a32 * fb2
    fb4 = A.a41 * fb1 + A.a42 * fb2
 

    logging.info('Plotting control ' + outName + '.png: ' + str(datetime.now()))
    draw((fb1, fb2, fb3, fb4), 0.4, (tt, tt, tt, tt), (-0.1, -0.1, -0.1, -0.1), (1.1, 1.1, 1.1, 1.1), outName, True)
    logging.info('Control ' + outName + '.png: plotted at ' + str(datetime.now()))

    return 1

def movie(dirName):
    """ make the movie with mencoder

    :param diName: string giving the name of the directory containing
                   the png files
    """

    logging.info('Starting Movie creation at ' + str(datetime.now()))


    os.system("mencoder 'mf://'" + dirName +"'/Solution_*.png' -mf type=png:fps=5 -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o " + dirName + "/film.mpg")

    logging.info('Movie created at ' + str(datetime.now()))

    return 1

def classical_movie(dirName):
    """ make the movie with mencoder

    :param diName: string giving the name of the directory containing
                   the png files
    """

    logging.info('Starting Movie creation at ' + str(datetime.now()))


    os.system("mencoder 'mf://'" + dirName +"'/Sol-classical*.png' -mf type=png:fps=5 -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o " + dirName + "/film-classical.mpg")

    logging.info('Movie created at ' + str(datetime.now()))

    return 1


def plot_solution(dirName, f, tt, domain, number_proc = 1):
    """ plot the solution in the simulation directory

    :param dirName: Name of the simulation directory
    :param f: flux class
    :param tt: numpy array with the times of the simulation
    :param domain: list of numpy array, containing the domain of the roads
    :param number_proc: number of processors
    """

    listaFiles = glob.glob(dirName + '/Solution_*.npz')

    # domain = (domain[0][1:-1], domain[1][1:-1], domain[2][1:-1])
    listOfLists = []
    
    for fileName in listaFiles:
        
        outName = os.path.splitext(fileName)[0]

        npzf = numpy.load(fileName)
        t = npzf['t']
        u = npzf['u']
        # (u1, u2, u3, u4) = u
        npzf.close()
        listOfLists.append([t, u, outName])
        
    logging.info('Plotting with ' + str(number_proc) + ' processors started at ' + str(datetime.now()))

    pool = Pool(processes = number_proc)
    res = [pool.apply_async(draw, args = (pp[1], pp[0], domain, (-0.1, -0.1, -0.1, -0.1), (1.1, 1.1, 1.1, 1.1), pp[2], False)) for pp in listOfLists]
    res = [pp.get() for pp in res]

    # draw((u1, u2, u3), t, domain, (-0.1, -0.1, -0.1), (1.1, 1.1, 1.1), outName, False)
    logging.info('Plotting with ' + str(number_proc) + ' processors finished at ' + str(datetime.now()))


    return 1


#
# plot_contour
#
def plot_contour(dirName, domain, colors):
    """ plot the contour of the solution

    :param dirName: Name of the directory of the simulation
    :param domain: list of numpy arrays containing the domains of the roads
    :param colors: int. Number of colors of the plot
    """

    listaFiles = glob.glob(dirName + '/Solution_*.npz')
    listaFiles.sort()

    
    domain = (domain[0][1:-1], domain[1][1:-1], domain[2][1:-1], domain[3][1:-1])
    uu1 = numpy.array([])
    uu2 = numpy.array([])
    uu3 = numpy.array([])
    uu4 = numpy.array([])
    tt = numpy.array([])
    
    for fileName in listaFiles:
        

        npzf = numpy.load(fileName)
        tt = numpy.append(tt, npzf['t'])
        u = npzf['u']
        uu1 = numpy.append(uu1, u[0][1:-1])
        uu2 = numpy.append(uu2, u[1][1:-1])
        uu3 = numpy.append(uu3, u[2][1:-1])
        uu4 = numpy.append(uu4, u[3][1:-1])
        npzf.close()

    uu1 = uu1.reshape((tt.size, domain[0].size))
    uu2 = uu2.reshape((tt.size, domain[1].size))
    uu3 = uu3.reshape((tt.size, domain[2].size))
    uu4 = uu4.reshape((tt.size, domain[3].size))

    draw_contour(domain, tt, (uu1, uu2, uu3, uu4), colors, dirName)



#
# plot_classic_solution
def plot_classic_solution(dirName, f, tt, domain, number_proc = 1):
    """ plot the solution in the simulation directory

    :param dirName: Name of the simulation directory
    :param f: flux class
    :param tt: numpy array with the times of the simulation
    :param domain: list of numpy array, containing the domain of the roads
    :param number_proc: number of processors
    """

    listaFiles = glob.glob(dirName + '/Sol-classical*.npz')

    # domain = (domain[0][1:-1], domain[1][1:-1], domain[2][1:-1])
    listOfLists = []
    
    for fileName in listaFiles:
        
        outName = os.path.splitext(fileName)[0]

        npzf = numpy.load(fileName)
        t = npzf['t']
        u = npzf['u']
        # (u1, u2, u3, u4) = u
        npzf.close()
        listOfLists.append([t, u, outName])
        
    logging.info('Plotting with ' + str(number_proc) + ' processors started at ' + str(datetime.now()))

    pool = Pool(processes = number_proc)
    res = [pool.apply_async(draw, args = (pp[1], pp[0], domain, (-0.1, -0.1, -0.1, -0.1), (1.1, 1.1, 1.1, 1.1), pp[2], False)) for pp in listOfLists]
    res = [pp.get() for pp in res]

    # draw((u1, u2, u3), t, domain, (-0.1, -0.1, -0.1), (1.1, 1.1, 1.1), outName, False)
    logging.info('Plotting with ' + str(number_proc) + ' processors finished at ' + str(datetime.now()))


    return 1


#
# plot_contour_classic
#
def plot_contour_classic(dirName, domain, colors):
    """ plot the contour of the solution

    :param dirName: Name of the directory of the simulation
    :param domain: list of numpy arrays containing the domains of the roads
    :param colors: int. Number of colors of the plot
    """

    listaFiles = glob.glob(dirName + '/Sol-classical*.npz')
    listaFiles.sort()

    
    domain = (domain[0][1:-1], domain[1][1:-1], domain[2][1:-1], domain[3][1:-1])
    uu1 = numpy.array([])
    uu2 = numpy.array([])
    uu3 = numpy.array([])
    uu4 = numpy.array([])
    tt = numpy.array([])
    
    for fileName in listaFiles:
        

        npzf = numpy.load(fileName)
        tt = numpy.append(tt, npzf['t'])
        u = npzf['u']
        uu1 = numpy.append(uu1, u[0][1:-1])
        uu2 = numpy.append(uu2, u[1][1:-1])
        uu3 = numpy.append(uu3, u[2][1:-1])
        uu4 = numpy.append(uu4, u[3][1:-1])
        npzf.close()

    uu1 = uu1.reshape((tt.size, domain[0].size))
    uu2 = uu2.reshape((tt.size, domain[1].size))
    uu3 = uu3.reshape((tt.size, domain[2].size))
    uu4 = uu4.reshape((tt.size, domain[3].size))

    draw_contour(domain, tt, (uu1, uu2, uu3, uu4), colors, dirName, True)

    return 1

#
# plot_outflow
#
def plot_outflow(DirName, A, tt):

    """ plot the outflow for the classical solution

    :param DirName: Name of the simulation directory
    :param A: class for the distribution matrix
    :param tt: numpy array with the times of the simulation
    """


    FileName = os.path.join(DirName, 'classical-outflow.npz')

    ff = numpy.load(FileName)
    outflow1 = ff['of1']
    outflow2 = ff['of2']
    ff.close()

    fb1 = outflow1
    fb2 = outflow2
    fb3 = A.a31 * outflow1 + A.a32 * outflow2
    fb4 = A.a41 * outflow1 + A.a42 * outflow2

    outName = os.path.join(DirName, 'classical-outflow')

    logging.info('Plotting control ' + outName + '.png: ' + str(datetime.now()))
    draw((fb1, fb2, fb3, fb4), 0.4, (tt, tt, tt, tt), (-0.1, -0.1, -0.1, -0.1), (1.1, 1.1, 1.1, 1.1), outName, True)
    logging.info('Control ' + outName + '.png: plotted at ' + str(datetime.now()))

    return 1
