#!/usr/bin/env python

#######################################
# Riemann_Problem.py
#
# Functions for finding solutions to
# Riemann Problems at 2x2 junctions
# 
#######################################

import numpy

#
# RP_product
#
def RP_product(rho1, rho2, rho3, rho4, f, A):
    """This function gives the solution for a Riemann problem in
    a 2x2 junction in case of the maximization of the product of the
    fluxes

    :param rho1: float. The density in the first incoming road
    :param rho2: float. The density in the second incoming road
    :param rho3: float. The density in the first outgoing road
    :param rho4: float. The density in the second outgoing road
    :param f: flux class.
    :param A: DistributionMatrix class.

    It returns a list (lf, ld). lf is a list of fluxes, while ld is a
    list of densities
    """

    f1 = demand(rho1, f)
    f2 = demand(rho2, f)
    f3 = supply(rho3, f)
    f4 = supply(rho4, f)

    points = list_of_points(f1, f2, f3, f4, A, False)

    values = [p[0] * p[1] * test(is_admissible(p, f1, f2, f3, f4, A)) for p in points]

    i = values.index(max(values)) # index of maximum

    # list of fluxes
    lf = (points[i][0], points[i][1], A.a31 * points[i][0] + A.a32 * points[i][1], 
          A.a41 * points[i][0] + A.a42 * points[i][1])

    # list of densities
    ld = (f.right_inverse(lf[0]), f.right_inverse(lf[1]),
          f.left_inverse(lf[2]), f.left_inverse(lf[3]))

    return (lf, ld)

#
# RP_sum
#
def RP_sum(rho1, rho2, rho3, rho4, f, A):
    """This function gives the solution for a Riemann problem in
    a 2x2 junction in case of the maximization of the sum of the
    fluxes

    :param rho1: float. The density in the first incoming road
    :param rho2: float. The density in the second incoming road
    :param rho3: float. The density in the first outgoing road
    :param rho4: float. The density in the second outgoing road
    :param f: flux class.
    :param A: DistributionMatrix class.


    It returns a list (lf, ld). lf is a list of fluxes, while ld is a
    list of densities
    """

    f1 = demand(rho1, f)
    f2 = demand(rho2, f)
    f3 = supply(rho3, f)
    f4 = supply(rho4, f)

    points = list_of_points(f1, f2, f3, f4, A)

    values = [sum(p) * test(is_admissible(p, f1, f2, f3, f4, A)) for p in points]

    i = values.index(max(values)) # index of maximum

    # list of fluxes
    lf = (points[i][0], points[i][1], A.a31 * points[i][0] + A.a32 * points[i][1], 
          A.a41 * points[i][0] + A.a42 * points[i][1])

    # list of densities
    ld = (f.right_inverse(lf[0]), f.right_inverse(lf[1]),
          f.left_inverse(lf[2]), f.left_inverse(lf[3]))

    return (lf, ld)

#
# demand function (increasing) for incoming roads
#
def demand(rho, f):
    """
    It is the demand function, used for incoming roads.
 
    :param rho: float. It is the density
    :param f: flux class.

    It returns the maximum flux for incoming roads
    """
    # assert rho >= f.rho_min and rho <= f.rho_max

    return numpy.maximum(f.flux(rho), numpy.sign(rho - f.theta) * f.maximum)
    

#
# supply function (decreasing) for outgoing roads
#
def supply(rho, f):
    """
    It is the supply function, used for outgoing roads.
 
    :param rho: float. It is the density
    :param f: flux class.

    It returns the maximum flux for outgoing roads
    """
    # assert rho >= f.rho_min and rho <= f.rho_max

    return numpy.maximum(f.flux(rho), numpy.sign(f.theta - rho) * f.maximum)
    

#
# test function
#
def test(cond):
    if cond:
        return 1.
    return 0.



#
# list_of_points function
#
def list_of_points(f1, f2, f3, f4, A, is_sum = True):
    """
    This function calculates the list of critical points 
    for the maximization problem.

    :param f1: float. Maximum flux in road 1.
    :param f2: float. Maximum flux in road 2.
    :param f3: float. Maximum flux in road 3.
    :param f4: float. Maximum flux in road 4.
    :param A: DistributionMatrix class.
    :param is_sum: Bool. If True (default) then we maximize the sum of the fluxes.
                         If False then we maximize the product of the fluxes.

    It returns the list of critical points.

    """

    points = []

    # intersection of constraints 1 and 2
    points.append((f1, f2))

    # intersection of constraints 1 and 3
    points.append((f1, f3/A.a32 - A.a31 * f1 /A.a32))

    # intersection of constraints 1 and 4
    points.append((f1, f4/A.a42 - A.a41 * f1 /A.a42))

    # intersection of constraints 2 and 3
    points.append((f3 / A.a31 - A.a32 * f2 / A.a31, f2))

    # intersection of constraints 2 and 4
    points.append((f4 / A.a41 - A.a42 * f2 / A.a41, f2))


    # intersection of constraints 3 and 4
    points.append((A.inverse[0][0] * f3 + A.inverse[0][1] * f4,
                   A.inverse[1][0] * f3 + A.inverse[1][1] * f4))

    if not is_sum: # case of the product
        # point in constraint 3
        points.append((f3 / A.a31, f3 / A.a32))

        # point in costraint 4
        points.append((f4 / A.a41, f4 / A.a42))
        
    return points


#
# is_admissible function
#
def is_admissible(point, f1, f2, f3, f4, A):
    """
    This function checks whether the point belongs to
    the admissible domain for the maximization problem.

    :param f1: float. Maximum flux in road 1.
    :param f2: float. Maximum flux in road 2.
    :param f3: float. Maximum flux in road 3.
    :param f4: float. Maximum flux in road 4.
    :param A: DistributionMatrix class.

    It returns a boolean.

    """

    error = .0000001
    f1 += error
    f2 += error
    f3 += error
    f4 += error

    return (point[0] <= f1 and point[1] <= f2 and
            A.a31 * point[0] + A.a32 * point[1] <= f3 and
            A.a41 * point[0] + A.a42 * point[1] <= f4)
    
