#!/usr/bin/env python

#######################################
# classical_junction_solution.py
#
# Function for the evaluation of a solution
# in a junction with the classical solver
# 
#######################################


import numpy
import Godunov
import save
import Riemann_Problem as RP


#
# Solution_Junction
#
def Solution_Junction(InitialDatum, domain, A, flux, tt, DirName, number_saving, is_sum = True):
    """
    This function calculates a initial-boundary value problem for the
    conservation law
    \pt u + \px f(u) = 0
    in all the junction and save the profiles. It uses the classical RP at the
    junction.
    
    :param InitialDatum: list of numpy arrays giving the initial condition
    :param domain: list of numpy arrays giving the space domain.
                   It has the same shape of InitialDatum
                   Each element has one ghost cell at the left and one at the right
    :param A: matrix distribution class
    :param flux: flux class
    :param tt: numpy array containing all the simulation times
    :param DirName: string giving the directory of the simulation
    :param number_saving: int. Gives the number
                          (possibly an upper bound) for
                          the number of savings
    :param is_sum: bool. If True (default) we maximize the sum,
                         if False, we maximize the product.
    """

    assert(InitialDatum[0].size == domain[0].size)

    number_saving = tt.size / number_saving 
    dx = domain[0][1] - domain[0][0]
    dt = tt[1] - tt[0]
    dtdx = dt/dx

    (u1, u2, u3, u4) = InitialDatum

    outflux1 = outflux2 = numpy.array([])

    if is_sum:
        (lf, ld) = RP.RP_sum(u1[-2], u2[-2], u3[1], u4[1], flux, A)
    else:
        (lf, ld) = RP.RP_product(u1[-2], u2[-2], u3[1], u4[1], flux, A)

    outflux1 = numpy.append(outflux1, lf[0])
    outflux2 = numpy.append(outflux2, lf[1])


    j = 0
    save.Sol_Save((u1, u2, u3, u4), tt[0], DirName + 'Sol-classical-', j) # saving the initial datum with ghost cells


    ttp = numpy.array([(i%number_saving) == 0 for i in xrange(tt.size)])
    ttp[-1] = True

    
    
    for i in xrange(1,tt.size):
        t = tt[i]

        u1 = Godunov.C_step(u1, ('f', ld[0]), flux, dtdx)
        u2 = Godunov.C_step(u2, ('f', ld[1]), flux, dtdx)
        u3 = Godunov.C_step(u3, (ld[2], 'f'), flux, dtdx)
        u4 = Godunov.C_step(u4, (ld[3], 'f'), flux, dtdx)

        if is_sum:
            (lf, ld) = RP.RP_sum(u1[-2], u2[-2], u3[1], u4[1], flux, A)
        else:
            (lf, ld) = RP.RP_product(u1[-2], u2[-2], u3[1], u4[1], flux, A)


        outflux1 = numpy.append(outflux1, lf[0])
        outflux2 = numpy.append(outflux2, lf[1])


        
        if ttp[i]:
            j += 1
            save.Sol_Save((u1, u2, u3, u4), tt[i], DirName + 'Sol-classical-', j)

    save.Outflux_Save(DirName + 'classical-outflow', outflux1, outflux2)
    return 1
# end of Solution_Junction
