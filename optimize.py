#!/usr/bin/env python

#######################################
# optimize.py
#
# 
# 
#######################################


import numpy
import os
import sys
import argparse
import logging
from datetime import datetime

path = os.path.join(os.getcwd(), "lib")
sys.path.insert(0, path)

import junction
import optimization


if __name__ == '__main__':

    desc = "optimize.py finds an optimal control"

    parser = argparse.ArgumentParser(description = desc, prog = "optimize.py")
    parser.add_argument('DirName', type=str, help="Enter the name of the directory")
    parser.add_argument('-c', '--cycles', type = int,
                         help = "Number of optimization cycles",
                        default = 1)
    parser.add_argument('-n', '--control_number', type = int, 
                        help = "Number for the control",
                        default = 0)
    parser.add_argument('--fc', dest='from_classic', 
                        action='store_true')
    parser.set_defaults(from_classic = False)
    parser.add_argument('--constant', dest = 'constant_control',
                        action = 'store_true')
    parser.set_defaults(constant_control = False)

    args = parser.parse_args()

    DirName = args.DirName
    cycles = args.cycles
    from_classic = args.from_classic
    constant_control = args.constant_control


    ParameterFile = os.path.join(DirName, 'parameters.py')
    execfile(ParameterFile)

    filelog = os.path.join(DirName, 'optimize_LOG.txt')

    try:
        os.remove(filelog)
    except OSError:
        pass


    logging.basicConfig(filename = filelog,
                        filemod = 'w', level = logging.DEBUG)
    
    A = junction.DistributionMatrix(A) # creation of distribution matrix class

    p = junction.Junction(m, n, roads, A, dx, InitialDatum) # creation of a junction class

    
    f = flux() # creation of the flux function

    if from_classic:
        control_number = 1
        optimization.classical_to_control(DirName, f)
    else:
        control_number = args.control_number
    
    
    if constant_control:
        M = 1
        control_number = 1
        optimization.classical_to_control(DirName, f)


    dt = 0.7 * dx / f.lam # time mesh according to the CFL condition
    tt = numpy.linspace(0., T + dt/2., num = T / dt, endpoint = True)


    logging.info('optimize.py with ' + str(cycles) + ' cycles started at ' + str(datetime.now()))


    if 'c_tv' in locals():
        optimization.optimization(DirName, tt, f, cycles, N, M, cost_J, p, c_tv, control_number)
    else:
        optimization.optimization(DirName, tt, f, cycles, N, M, cost_J, p, 0, control_number)

    logging.info('optimize.py with ' + str(cycles) + ' cycles finished at ' + str(datetime.now()))
    
