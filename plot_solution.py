#!/usr/bin/env python
###
### plot_solution.py
### plots the solution computed for a given control
### and make the movie


import numpy
## import matplotlib
## import matplotlib.pyplot
import os
import glob
import sys
import argparse
import logging
from datetime import datetime


path = os.path.join(os.getcwd(), "lib")
sys.path.insert(0, path)

# import road
import junction
import ibvp
import plotting
import cost

def check_negative(value):
    """ check if a value is negative"""
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid entry" % value)

    return ivalue


if __name__ == '__main__':


    desc = "plot_solution.py performs the plot" + \
           "of the solution and make the movie"

    parser = argparse.ArgumentParser(description = desc, prog = "plot_solution.py")
    parser.add_argument('DirName', type=str, help="Enter the name of the directory")
    parser.add_argument('-p', '--processors', type = check_negative, help="Enter the number of working processors", default = 1)
    parser.add_argument('-c', '--control', type = str, help="Enter the control name", default = "controls_000.npz")
    parser.add_argument('-n', '--numbers_figures', type = check_negative,
                        help = "Enter the lower bound for the number of figures (frames of the movie)",
                        default = 80)
    

    args = parser.parse_args()
    NumberProcessors = args.processors
    DirName = args.DirName
    Control_Name = DirName +  os.path.splitext(args.control)[0] + '.npz'
    Number_Figures = args.numbers_figures

    # Reads all parameters, Initial Datum, Flow and MaxCharSpeed
    execfile(DirName + "/parameters.py")

    logging.basicConfig(filename = DirName + '/plot_solution_LOG.txt',
                        filemod = 'w', level = logging.DEBUG)

    
    npzf = numpy.load(Control_Name)
    control = npzf['c']
    npzf.close()
    
    A = junction.DistributionMatrix(A)
    junc = junction.Junction(m, n, roads, A, dx, InitialDatum)
    f = flux()

    dt = 0.7 * dx / f.lam
    tt = numpy.linspace(0., T + dt/2., num = T / dt, endpoint = True)

    
    domain = (junc.incoming[0].domain, junc.incoming[1].domain, junc.outgoing[0].domain, junc.outgoing[1].domain)
    InitialDatum = (junc.incoming[0].initial_datum, junc.incoming[1].initial_datum, junc.outgoing[0].initial_datum, junc.outgoing[1].initial_datum)

    
    FileName = DirName + 'Solution_'

    logging.info('plot_solution.py with ' + str(NumberProcessors) + ' processors on file ' + Control_Name +' started at ' + str(datetime.now()))


    ibvp.Solution_Junction(InitialDatum, junc, domain, junc.matrix, f, tt, control, FileName, Number_Figures)

    plotting.plot_solution(DirName, f, tt, domain, NumberProcessors)

    plotting.movie(DirName)    

    ff = open(DirName + 'cost.txt', 'w')
    cc = cost.find_cost(junc, control, A, tt, f, cost_J)
    ff.write('Cost = ' + str(cc) + '\n')

    cc1 = cost.find_tv(junc, control, A, tt, f)
    for i in xrange(len(cc1)):
        ff.write('TV flux ' + str(i+1) + ' = ' + str(cc1[i]) + '\n')

    if 'c_tv' not in locals():
        c_tv = 0.
    ff.write('Final cost = ' + str(cc - c_tv * sum(cc1)) + '\n')
    ff.close()

    logging.info('plot_solution.py with ' + str(NumberProcessors) + ' processors on file ' + Control_Name + ' finished at ' + str(datetime.now()))
    





