# parameters

######################################################
## Roads definitions
######################################################

m = 2 # number of incoming roads
n = 2 # number of outgoing roads

roads = [(-5, 0), (-5, 0), (0, 5), (0,5)] # list of roads

######################################################
## MESH SIZE
######################################################

dx = 0.05 # spatial mesh dx

######################################################
## TIME
######################################################

T = 1.1 # time T
M = 20 # number of intervals where the control is constant
N = 20 # number of mesh points for the control range


######################################################
## INITIAL DATA
######################################################

def temp(x):
    x = numpy.asarray(x)
    y = numpy.zeros(x.shape)
    y += (x >= -.5) * 0.5
    y += ((x >= -1.) & (x < -0.5)) * 0.
    y += ((x >= -1.5) & (x < -1.)) * 0.25
    y += ((x >= -2.) & (x < -1.5)) * 0.
    y += (x < -2.) * .25

    return y

# Initial Datum 
InitialDatum = [lambda x: temp(x),
                lambda x: 0.5 * numpy.ones_like(x),
                lambda x: 0.1 * numpy.ones_like(x),
                lambda x: 0.1 * numpy.ones_like(x)]


######################################################
## DISTRIBUTION MATRIX
######################################################

# Matrix A [[a_31, a_32], [a_41, a_42]]
A = [[.5, .3], [.5, .7]]



######################################################
## FLUX class
######################################################

class flux(object):

    def __init__(self):
        self.theta = 0.5 # point of maximum for the flux
        self.rho_min = 0. # point of minimum 
        self.rho_max = 1. # point of maximum
        self.lam = numpy.amax([self.derivative(self.rho_min),
                               - self.derivative(self.rho_max)])
        self.maximum = self.flux(self.theta)

        
    def flux(self, u):
        return 4. * u * (1. - u)

    def left_inverse(self, f):
        f = numpy.minimum(f, self.maximum)
        f = numpy.maximum(f, 0.)
        return 0.5 * (1. - numpy.sqrt(1. - f)) 

    def right_inverse(self, f):
        f = numpy.minimum(f, self.maximum)
        f = numpy.maximum(f, 0.)
        return 0.5 * (1. + numpy.sqrt(1. - f)) 

    def derivative(self, u):
        return 4. - 8. * u


######################################################
## COST
######################################################

# cost function J
def cost_J(u):
    # u is a tuple or list composed by numpy arrays
    # of the same size
    return 50. * reduce(numpy.multiply, u)

c_tv = 0.2
######################################################
## PLOTTING RANGES
######################################################


