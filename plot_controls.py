#!/usr/bin/env python
###
### plot_controls.py
### plots the controls 



import numpy
import os
import sys
import argparse
import logging

path = os.path.join(os.getcwd(), "lib")
sys.path.insert(0, path)

import plotting
import junction


                
if __name__ == '__main__':


    desc = "plot_controls.py performs the plot of the controls"

    parser = argparse.ArgumentParser(description = desc, prog = "plot_controls.py")
    parser.add_argument('DirName', type=str, help="Enter the name of the directory")
    parser.add_argument('-c', '--control', type = str, help="Enter the control name")

    args = parser.parse_args()
    DirName = args.DirName

    # Reads all parameters
    execfile(DirName + "/parameters.py")

    A = junction.DistributionMatrix(A)
    junc = junction.Junction(m, n, roads, A, dx, InitialDatum)

    logging.basicConfig(filename = os.path.join(DirName, 'plot_controls_LOG.txt'),
                        filemod = 'w', level = logging.DEBUG)

    f = flux()
    dt = 0.7 * dx / f.lam
    tt = numpy.linspace(0., T + dt/2., num = T / dt, endpoint = True)

    if args.control:

        FileName = DirName + os.path.splitext(args.control)[0] + '.npz'
        plotting.plot_control(FileName, junc, f, A, tt)
        
    else:
        plotting.plot_controls(DirName, junc, f, A, tt)
