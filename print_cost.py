#!/usr/bin/env python
###
### print_cost.py
### prints the cost computed for a given control


import numpy
import os
import sys
import argparse


path = os.path.join(os.getcwd(), "lib")
sys.path.insert(0, path)

import junction
import cost


def summary(DirName):
    """ print the summary of the costs"""

    f = open(DirName + 'Cost.txt', 'w')

    dir1 = ['product/', 'sum/']
    for name in dir1:
        f.write('Case of ' + name +'\n\n')

        ll = [x[0] for x in os.walk(DirName + name) if x[0] != DirName + name]
        ll.sort()
        for l in ll:
            f1 = open(l + '/cost.txt', 'r')
            s1 = f1.read()
            f1.close()
            f1 = open(l + '/cost-classical.txt', 'r')
            s2 = f1.read()
            f1.close()
            f.write('Subdir: ' + l + '\n') 
            f.write(s1)
            f.write('\n')
            f.write('Cost for the classical solution\n')
            f.write(s2)
            f.write('\n\n\n')

    return 1



if __name__ == '__main__':


    desc = "print_cost.py prints the cost" + \
           " of the solution"

    parser = argparse.ArgumentParser(description = desc, prog = "print_cost.py")
    parser.add_argument('DirName', type=str, help="Enter the name of the directory")
    parser.add_argument('-c', '--control', type = str, help="Enter the control name", default = "controls_000.npz")
    parser.add_argument('-s', '--summary', action="count", help="gives the summary of the cost")

    args = parser.parse_args()
    DirName = args.DirName

    if args.summary:
        summary(DirName)
        exit(1)


    Control_Name = DirName +  os.path.splitext(args.control)[0] + '.npz'

    # Reads all parameters, Initial Datum, Flow and MaxCharSpeed
    execfile(DirName + "/parameters.py")

    
    npzf = numpy.load(Control_Name)
    control = npzf['c']
    npzf.close()
    
    A = junction.DistributionMatrix(A)
    junc = junction.Junction(m, n, roads, A, dx, InitialDatum)
    f = flux()

    dt = 0.7 * dx / f.lam
    tt = numpy.linspace(0., T + dt/2., num = T / dt, endpoint = True)

    
    ff = open(DirName + 'cost.txt', 'w')

    cc = cost.find_cost(junc, control, A, tt, f, cost_J)
    print('Cost = ' + str(cc) + '\n')
    ff.write('Cost = ' + str(cc) + '\n')


    cc1 = cost.find_tv(junc, control, A, tt, f)
    for i in xrange(len(cc1)):
        print('TV flux ' + str(i+1) + ' = ' + str(cc1[i]) + '\n')
        ff.write('TV flux ' + str(i+1) + ' = ' + str(cc1[i]) + '\n')

    if 'c_tv' not in locals():
        c_tv = 0.
    print('Final cost = ' + str(cc - c_tv * sum(cc1)) + '\n')
    ff.write('Final cost = ' + str(cc - c_tv * sum(cc1)) + '\n')

    

    ff.close()


