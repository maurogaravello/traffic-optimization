#!/usr/bin/env python
###
### classical_solution.py







### plots the solution computed for a given control
### and make the movie


import numpy
import os
import glob
import sys
import argparse
import logging
from datetime import datetime


path = os.path.join(os.getcwd(), "lib")
sys.path.insert(0, path)

import junction
import ibvp
import plotting
import cost
import classical_junction_solution as cjs

def check_negative(value):
    """ check if a value is negative"""
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid entry" % value)

    return ivalue


if __name__ == '__main__':


    desc = "classical_solution.py performs the calculus and the plot" + \
           "of the classical solution and make the movie"

    parser = argparse.ArgumentParser(description = desc, prog = "classical_solution.py")
    parser.add_argument('DirName', type=str, help="Enter the name of the directory")
    parser.add_argument('-p', '--processors', type = check_negative, help="Enter the number of working processors", default = 1)
    parser.add_argument('-n', '--numbers_figures', type = check_negative,
                        help = "Enter the lower bound for the number of figures (frames of the movie)",
                        default = 80)
    parser.add_argument('-c', '--colors', type = int, help = 'Enter the number of colors for the contour plot', default = 256)
    

    args = parser.parse_args()
    NumberProcessors = args.processors
    DirName = args.DirName
    Number_Figures = args.numbers_figures
    Colors = args.colors

    # Reads all parameters, Initial Datum, Flow and MaxCharSpeed
    execfile(os.path.join(DirName, "parameters.py"))

    logging.basicConfig(filename = DirName + '/classical_solution_LOG.txt',
                        filemod = 'w', level = logging.DEBUG)

    logging.info('classical_solution.py with ' + str(NumberProcessors) + ' processors started at ' + str(datetime.now()))

    
    
    A = junction.DistributionMatrix(A)
    junc = junction.Junction(m, n, roads, A, dx, InitialDatum)
    f = flux()

    dt = 0.7 * dx / f.lam
    tt = numpy.linspace(0., T + dt/2., num = T / dt, endpoint = True)

    
    domain = (junc.incoming[0].domain, junc.incoming[1].domain, junc.outgoing[0].domain, junc.outgoing[1].domain)
    InitialDatum = (junc.incoming[0].initial_datum, junc.incoming[1].initial_datum, junc.outgoing[0].initial_datum, junc.outgoing[1].initial_datum)

    if cost_J(([1.], [2.])) > 2.5:
        is_sum = True
    else:
        is_sum = False


    cjs.Solution_Junction(InitialDatum, domain, A, f, tt, DirName, Number_Figures, is_sum)
    plotting.plot_classic_solution(DirName, f, tt, domain, NumberProcessors)
    plotting.classical_movie(DirName)    

    ff = open(DirName + 'cost-classical.txt', 'w')
    cc = cost.find_classical_cost(junc, A, f, DirName, tt, cost_J)
    ff.write('Cost = ' + str(cc[0]) + '\n')

    for i in xrange(1, len(cc)):
        ff.write('TV flux ' + str(i) + ' = ' + str(cc[i]) + '\n')

    if 'c_tv' not in locals():
        c_tv = 0.
    ff.write('Final cost = ' + str(cc[0] - c_tv * (cc[1] + cc[2])) + '\n')
    ff.close()

    plotting.plot_contour_classic(DirName, domain, Colors)
    plotting.plot_outflow(DirName, A, tt)

    logging.info('classical_solution.py with ' + str(NumberProcessors) + ' processors finished at ' + str(datetime.now()))
    
