#!/usr/bin/env python
###
### plot_contour.py
### plots the countour for the solution
### computed for a given control


import numpy
import os
import sys
import argparse
import logging
from datetime import datetime


path = os.path.join(os.getcwd(), "lib")
sys.path.insert(0, path)

# import road
import junction
import ibvp
import plotting

def check_negative(value):
    """ check if a value is negative"""
    ivalue = int(value)
    if ivalue <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid entry" % value)

    return ivalue


if __name__ == '__main__':


    desc = "plot_contour.py performs the plot" + \
           " of the contour of the solution"

    parser = argparse.ArgumentParser(description = desc, prog = "plot_contour.py")
    parser.add_argument('DirName', type=str, help="Enter the name of the directory")
    parser.add_argument('-c', '--colors', type = check_negative, help="Enter the number of working processors", default = 256)
    

    args = parser.parse_args()
    NumberColors = args.colors
    DirName = args.DirName

    # Reads all parameters, Initial Datum, Flow and MaxCharSpeed
    execfile(DirName + "/parameters.py")

    logging.basicConfig(filename = DirName + '/plot_contour_LOG.txt',
                        filemod = 'w', level = logging.DEBUG)

    A = junction.DistributionMatrix(A)
    junc = junction.Junction(m, n, roads, A, dx, InitialDatum)

    
    domain = (junc.incoming[0].domain, junc.incoming[1].domain,
              junc.outgoing[0].domain, junc.outgoing[1].domain)


    logging.info('plot_contour.py with ' + str(NumberColors) + ' colors started at ' + str(datetime.now()))


    plotting.plot_contour(DirName, domain, NumberColors)

    
    logging.info('plot_contour.py with ' + str(NumberColors) + ' colors finished at ' + str(datetime.now()))
    





